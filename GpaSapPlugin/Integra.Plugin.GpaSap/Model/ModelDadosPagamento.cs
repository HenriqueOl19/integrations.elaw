﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integra.Plugin.GpaSap.Model
{

    public class DadosPagamentosModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public ItemDadosPagamentoModel ItemDadoPagamentos { get; set; }
    }

    public class ItemDadosPagamentoModel
    {
        public string classType { get; set; }
        public string description { get; set; }
        public string codigoProcessoValorTipo { get; set; }
        public string nomeFavorecido { get; set; }
        public string tpDocumento { get; set; }
        public string cpfCnpjFavorecido { get; set; }
        public string processoId { get; set; }
        public string codLoja { get; set; }
        public string kidno { get; set; }
        public string valor { get; set; }
        public string txtContabil { get; set; }
        public string contaContabil { get; set; }
        public string dataVencimento { get; set; }
        public string verificarPagamento31 { get; set; }
        public string verificarPagamento581 { get; set; }
        public string area { get; set; }
        public string chaveBanco { get; set; }
        public string agencia { get; set; }
        public string conta { get; set; }
        public string digito { get; set; }
        public string numeroIdJudicial { get; set; }
        public string codigoBarras { get; set; }
        public string grupo { get; set; }
        
    }
}

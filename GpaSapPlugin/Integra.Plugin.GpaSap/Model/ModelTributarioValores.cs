﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integra.Plugin.GpaSap
{
    public class ModelTributarioValores
    {
        public decimal Valor { get; set; }
        public long CostCenterId { get; set; }
        public long ProcessoValorId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integra.Plugin.GpaSap.Model
{
    public class ModelProcessosValor_a_Integrar
    {
        public long ProceasoValorId { get; set; }
        public DateTime DtHrRegistrado { get; set; }
        public DateTime DtHrMontagemArquivo { get; set; }
        public DateTime DtHrVerificacaoArquivo { get; set; }
        public DateTime DtHrConfirmacaoWorkflow { get; set; }
        public string ClassType { get; set; }
        public string Versao { get; set; }
        public bool isSent { get; set; }
        public long ProcessoId { get; set; }
        public long LoteIntegracaoId { get; set; }
        public string Message { get; set; }
        public ItemDadosPagamentoModel itemPagamento { get; set; }

    }
}

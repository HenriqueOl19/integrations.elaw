﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using Integra.Plugin.GpaSap.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net.WebSockets;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace Integra.Plugin.GpaSap
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("DisplayName", "MontarArquivoEnvio")]
    [ExportMetadata("Description", "Montar Arquivo txt contendo os pagamentos, para enviar via FTP")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "GPA_SAP_MontarArquivosEnvio")]
    [ExportMetadata("Status", PluginStatus.Ativo)]


    public class MontarArquivoEnvio : IPlugin
    {
        private readonly string host = "https://localhost:6001";

        public void Execute(RequisicaoModel requisicao)
        {

            Console.WriteLine($"Executando plugin MontarArquivoEnvio, req id: {requisicao.Id.ToString()}");
            MontarArquivo();
            
           

            

        }

        public void MontarArquivo()
        {
            var txtArquivo = "";
            List<ModelProcessosValor_a_Integrar> ProcessoValorParaIntegrar = null;
            foreach (ModelProcessosValor_a_Integrar item in ProcessoValorParaIntegrar)
            {
                txtArquivo += MontarPagamento(item.ProceasoValorId,item.itemPagamento);
            }

        }



        public String MontarPagamento(long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)

        {

            //SELECTS globais
         
            //Variaveis
            var PagamentoLinha = Aba10(123, 1, itemDadosPagamento);
            bool isGarantia = itemDadosPagamento.classType == "processoValorGarantia" ? true : false;


            //Montagem
            if (itemDadosPagamento.verificarPagamento581 != "") //sim
            {
                PagamentoLinha = Aba30_31(PagamentoLinha, "30", ProcessoValorId,itemDadosPagamento);
            }

            if (itemDadosPagamento.verificarPagamento31 != "")
            {
                PagamentoLinha = Aba30_31(PagamentoLinha, "31", ProcessoValorId,itemDadosPagamento);
            }

            PagamentoLinha = Aba50_4(Aba50_3(Aba50_2(Aba50_1(PagamentoLinha, ProcessoValorId,itemDadosPagamento), ProcessoValorId,itemDadosPagamento), ProcessoValorId,itemDadosPagamento), ProcessoValorId,itemDadosPagamento);

            var verificarPagamento50 = itemDadosPagamento.verificarPagamento581 == "985" ? true:false ;
            if (isGarantia == false && verificarPagamento50 == true)
            {
                PagamentoLinha = Aba50_5(PagamentoLinha, ProcessoValorId,itemDadosPagamento);
            }

            PagamentoLinha = Aba60_1(PagamentoLinha, ProcessoValorId, itemDadosPagamento);

            var tributario = itemDadosPagamento.area; 

            if (tributario != "")//verificar (tributario != "") 
            {
                List<ModelTributarioValores> valorRateios = null; //select replace(TO_char(VALOR, '0000000000000d99'), '.', ''), COST_CENTER_ID, PROCESSO_VALOR_ID from {PROCESSO_VALOR_RATEIO} where PROCESSO_VALOR_ID = @PrcValorId

                foreach (ModelTributarioValores item in valorRateios)
                {
                    PagamentoLinha = Aba60_2_Trib(PagamentoLinha, ProcessoValorId, isGarantia, itemDadosPagamento);
                }
            }
            else
            {
                PagamentoLinha = Aba60_2(PagamentoLinha, ProcessoValorId, isGarantia, itemDadosPagamento);
            }

            return PagamentoLinha;


        }


        /// <summary>
        /// Função que inicia a criação do arquivo, a primeira linha
        /// </summary>
        /// <param name="ProcessoValorId">Pagamento ID</param>
        /// <param name="Versao"> Quantas vezes o pagamento foi enviado</param>
        /// <param name="VerificarPagamento31">Verifica se o elaw field 31 existe</param>
        /// <returns></returns>
        public String Aba10(long ProcessoValorId, int Versao, ItemDadosPagamentoModel itemDadosPagamento)
        {
            string linha = "10000000";
            string codigo = itemDadosPagamento.codigoProcessoValorTipo;

            //009-011
            if (codigo != "")
            {
                if (codigo.Length == 1)
                {
                    linha = String.Concat(linha, "00", codigo);
                }
                else
                {
                    linha = String.Concat(linha, "0", codigo);
                }
            }
            else
            {
                linha = String.Concat(linha, " ", " ", " ");
            }

            //012-031
            linha = String.Concat(linha, "DESEMBOLSO", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

            //032-047
            string AuxProcValorIdVersao = String.Concat(ProcessoValorId, "-", Versao);
            AuxProcValorIdVersao = AuxProcValorIdVersao.PadRight(16, ' ');
            linha = string.Concat(linha, AuxProcValorIdVersao);

            string TpCpfCnpj = itemDadosPagamento.tpDocumento;
            string cpfCnpj = itemDadosPagamento.cpfCnpjFavorecido;

            //048-063
            switch (TpCpfCnpj)
            {
                case "CNPJ":
                    cpfCnpj = cpfCnpj.PadLeft(14, '0');
                    linha = String.Concat(linha, " ", cpfCnpj, 'J');
                    break;
                case "CPF":
                    cpfCnpj = cpfCnpj.PadLeft(11, '0');
                    linha = String.Concat(linha, " ", cpfCnpj, 'F');
                    break;
                default:
                    cpfCnpj = cpfCnpj.PadLeft(14, '0');
                    linha = String.Concat(linha, " ", cpfCnpj, 'E');
                    break;
            }

            //064-088
            var ProcessoId = itemDadosPagamento.processoId;
            linha = string.Concat(linha + ProcessoId);

            linha = linha.PadRight(88, ' ');

            var CodLoja = itemDadosPagamento.codLoja; 
         
            //089-099
            CodLoja = CodLoja.PadLeft(10, '0');
            linha = String.Concat(linha, CodLoja, ' ');

            //100-115
            linha = String.Concat(linha, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd"));

            //116-131
            linha = String.Concat(linha, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

            //132-155
            linha = String.Concat(linha, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

            //156-160
            linha = String.Concat(linha, "BRL", ' ', ' ');

            //161-175
            linha = String.Concat(linha, "00000000000000", ' ');

            if (itemDadosPagamento.verificarPagamento31.ToUpper() != "SIM")
            {
                var kidno = itemDadosPagamento.kidno.Trim();
                if (kidno != "")
                {
                    //176-199
                    linha = String.Concat(linha, kidno.Substring(0, 23));
                }
            }

            linha = linha.PadRight(199, ' ');

            linha = String.Concat(linha, "\n");

            return linha;
        }


        /// <summary>
        /// Função utilizada para inserir linha do formato 30 ou 31
        /// </summary>
        /// <param name="Aba10">linha anterior que deseja ser concatenada com a nova</param>
        /// <param name="NumeroAba">Coloque 30 para o formato 30 e 31 para o formato 31</param>
        /// <returns></returns>
        public static String Aba30_31(String Aba10, String NumeroAba, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = NumeroAba;

            //003-005
            linha += "BR" + ' ';

            var ChaveBanco = itemDadosPagamento.chaveBanco;

            //006-009
            switch (ChaveBanco)
            {
                case "1":
                    linha += "00" + ChaveBanco + "*";
                    break;
                case "2":
                    linha += "0" + ChaveBanco + "*";
                    break;
                default:
                    linha += ChaveBanco + "*";
                    break;
            }


            var Agencia = itemDadosPagamento.agencia;
            var Digito = itemDadosPagamento.digito;  

            //010-020
            linha += Agencia;

            linha = linha.PadRight(20, ' ');

            var Conta = itemDadosPagamento.conta;

            //021-040
            linha += Conta + Digito;

            linha = linha.PadRight(60, ' ');

            linha += "\n";

            return Aba10 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 50_1
        /// </summary>
        /// <param name="Aba30_31">linha com o formato 30 ou 31</param>
        /// <returns></returns>
        public String Aba50_1(string Aba30_31, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "50";

            //003-006
            linha += "NOTA";

            //007-009
            linha += "001";

            var usuario = itemDadosPagamento.nomeFavorecido;

            //010-081
            linha += "ELAW/" + usuario.Substring(0, 67);

            linha = linha.PadRight(80, ' ');
            linha += "\n";

            return Aba30_31 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 50_2
        /// </summary>
        /// <param name="Aba50_1">linha com o formato 50_1</param>
        /// <returns></returns>
        public String Aba50_2(string Aba50_1, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "50";

            //003-006
            linha += "INFO";

            //007-009
            linha += "001";

            var areaDireito = itemDadosPagamento.area.ToUpper(); 

            //010-081
            linha += "ELAW/" + areaDireito.Substring(0, 67);

            linha = linha.PadRight(80, ' ');
            linha += "\n";

            return Aba50_1 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 50_3
        /// </summary>
        /// <param name="Aba50_2">linha com o formato 50_2</param>
        /// <returns></returns>
        public String Aba50_3(string Aba50_2, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "50";

            //003-006
            linha += "INFO";

            //007-009
            linha += "002";

            var nomeFavorecido = itemDadosPagamento.nomeFavorecido.ToUpper();

            //010-081
            linha += "ELAW/" + nomeFavorecido.Substring(0, 67);

            linha = linha.PadRight(80, ' ');
            linha += "\n";

            return Aba50_2 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 50_4
        /// </summary>
        /// <param name="Aba50_3">linha com o formato 50_3</param>
        /// <returns></returns>
        public String Aba50_4(string Aba50_3, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "50";

            //003-006
            linha += "INFO";

            //007-009
            linha += "003";

            var desccricaoProcV = itemDadosPagamento.description.ToUpper();

            //010-081
            linha += "ELAW/" + desccricaoProcV.Substring(0, 67);

            linha = linha.PadRight(80, ' ');
            linha += "\n";

            return Aba50_3 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 50_5
        /// </summary>
        /// <param name="Aba50_4">linha com o formato 50_4</param>
        /// <returns></returns>
        public String Aba50_5(string Aba50_4, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "50";

            //003-006
            linha += "INFO";

            //007-009
            linha += "004";

            var numeroIdJudicial = itemDadosPagamento.kidno; //select VALUE_LONG from {EL_FIELD_VALUE} where EL_FIELD_ID = 506 and processo_valor_id in (@ProcValorId)

            //010-???
            linha += "ELAW/" + numeroIdJudicial;

            linha = linha.PadRight(80, ' ');
            linha += "\n";

            return Aba50_4 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 60_1
        /// </summary>
        /// <param name="Aba50_5">linha com o formato 50_5</param>
        /// <param name="VerificaPagamento31">Campo de retorno do banco de dados</param>
        /// <returns></returns>
        public String Aba60_1(string Aba50_5, long ProcessoValorId, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "60";

            //003-010
            linha += "BRUTO" + ' ' + ' ';

            //007-009
            linha += "004";


            //011-025
            linha += itemDadosPagamento.valor;


            //026-033
            linha += itemDadosPagamento.dataVencimento;

            //034-064
            linha += "0000000000000000000000000000000";


            //065-114
            var _txtContabil = itemDadosPagamento.txtContabil.PadRight(44, ' ');
            linha += "e-law" + _txtContabil;

            //115-140
            linha += ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ' + ' ';

            if (itemDadosPagamento.verificarPagamento31.ToUpper() != "SIM")//(VerificaPagamento31 == "")
            {
                var codBarras = itemDadosPagamento.codigoBarras.Remove('.').Remove(' '); 
                if (codBarras != "")
                {
                    //141-188
                    linha += codBarras;
                }
            }

            linha = linha.PadRight(188, ' ');

            var meioPagamento = itemDadosPagamento.grupo.ToUpper();

            //189-189
            linha += meioPagamento;

            linha = linha.PadRight(193, ' ');

            linha += "\n";

            return Aba50_5 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 60_2_Trib, quando existe Valores Rateio
        /// </summary>
        /// <param name="Aba60_1">Linha com o formato 60_1</param>
        /// <param name="ProcessoValorId"></param>
        /// <param name="TxtContabil"></param>
        /// <param name="Valor"></param>
        /// <param name="DataVenc"></param>
        /// <param name="CostCenterId"></param>
        /// <param name="isGarantia"></param>
        /// <returns></returns>
        public string Aba60_2_Trib(String Aba60_1, long ProcessoValorId, bool isGarantia, ItemDadosPagamentoModel itemDadosPagament)
        {
            //001-002
            var linha = "60";

            //003-010
            linha += "RATCONT" + ' ' + ' ';

            //011-025
            linha += itemDadosPagament.valor;

            //026-033
            linha += itemDadosPagament.dataVencimento;

            //034-048
            linha += "000000000000000";

            var contaContabil = itemDadosPagament.contaContabil; 
            //049-054
            linha += contaContabil;

            var codLoja = itemDadosPagament.codLoja; 
            codLoja = codLoja.PadLeft(10, '0');

            //055-064
            linha += codLoja;

            //065-114
            var _txtContabil = itemDadosPagament.txtContabil.PadRight(44, ' ');
            linha += "e-law" + _txtContabil;

            linha = linha.PadRight(127, ' ');

            if (isGarantia)
            {
                //128-130
                linha += "001";
            }
            linha = linha.PadRight(193, ' ');
            linha += "\n";
            return Aba60_1 + linha;
        }

        /// <summary>
        /// Função para inserir linha do formato 60_2
        /// </summary>
        /// <param name="Aba60_1"></param>
        /// <param name="ProcessoValor"></param>
        /// <param name="TxtContabil"></param>
        /// <param name="ValorPagamento"></param>
        /// <param name="DataVencimento"></param>
        /// <param name="isGarantia"></param>
        /// <returns></returns>
        public string Aba60_2(string Aba60_1, long ProcessoValor, bool isGarantia, ItemDadosPagamentoModel itemDadosPagamento)
        {
            //001-002
            var linha = "60";

            //003-010
            linha += "RATCONT" + ' ' + ' ';

            //011-025
            linha += itemDadosPagamento.valor;

            //026-033
            linha += itemDadosPagamento.dataVencimento;

            //034-048
            linha += "000000000000000";

            var contaContabil = itemDadosPagamento.contaContabil; 

            //049-054
            linha += contaContabil;

            var codLoja = itemDadosPagamento.codLoja; 


            codLoja = codLoja.PadLeft(10, '0');

            //055-064
            linha += codLoja;

            //065-114
            var _txtContabil = itemDadosPagamento.txtContabil.PadRight(44, ' ');
            linha += "e-law" + _txtContabil;

            linha = linha.PadRight(127, ' ');

            if (isGarantia)
            {
                //128-130
                linha += "001";
            }
            linha = linha.PadRight(193, ' ');
            linha += "\n";
            return Aba60_1 + linha;

        }


        public DadosPagamentosModel ChamarServiçoBD (int processoValorId, RequisicaoModel requisicao)
        {
            try
            {
                HttpHelper http = new HttpHelper(new WebClientBase());

                var resultadoConsulta = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "RealizarQueryByProcessoValorID"), new List<ExternalValues>()
            {
                new ExternalValues("{processoValorId}", processoValorId.ToString()),
            });

                requisicao.ConteudoRetorno = new RequisicaoDataModel()
                {
                    TipoConteudo = "application/json",
                    Conteudo = resultadoConsulta.Json
                };

               //Poe o json no objeto
                DadosPagamentosModel dadosPgto = JsonConvert.DeserializeObject<DadosPagamentosModel>(resultadoConsulta.Json.ToString());
               
                return dadosPgto;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private IList<HttpStep> StepsConfig()
        {
            var steps = new List<HttpStep>();

            steps.Add(new HttpStep("RealizarQueryByProcessoValorID", host+"/api/gpa/gpasap/GetInfoByProcessoValorId?processoValorId={processoValorId}", "GET", "UTF-8", HttpResultType.Json)
            {
                ContentType = "application/json"
            });

            return steps;
        }
    }




}

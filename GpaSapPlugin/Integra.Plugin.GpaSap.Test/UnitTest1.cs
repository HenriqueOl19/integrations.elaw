using Integra.Common.Model;
using NUnit.Framework;
using System;
using System.Dynamic;

namespace Integra.Plugin.GpaSap.Test
{
    public class Tests
    {
        private MontarArquivoEnvio _montarArquivoEnvio;
        [SetUp]
        public void Setup()
        {
            _montarArquivoEnvio = new MontarArquivoEnvio();
        }

        [Test]
        public void TesteBancoDados()
        {
            var requisicao = new RequisicaoModel
            {
                Id = "1234",
                DataCadastro = DateTime.Now,
                Autenticacao = new RequisicaoAutenticacaoModel() { Token = "6578987f1-fe4e-7ui9-76y3-3at6yhsf9rd4" },
            };


            dynamic dadosConsulta = new ExpandoObject();
            dadosConsulta.ProcessoValorId = 624991;

            requisicao.ConteudoEntrada = new RequisicaoDataModel()
            {
                TipoConteudo = "ProcessoValorId",
                Conteudo = 624991
            };

            _montarArquivoEnvio.Execute(requisicao);



        }
    }
}
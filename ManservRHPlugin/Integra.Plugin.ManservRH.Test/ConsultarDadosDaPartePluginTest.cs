using Integra.Common.Model;
using NUnit.Framework;
using System;
using System.Dynamic;

namespace Integra.Plugin.ManservRH.Test
{
    public class Tests
    {
        private ConsultarDadosDaPartePlugin Plugin;

        [SetUp]
        public void Setup()
        {
            Plugin = new ConsultarDadosDaPartePlugin();
        }

        [Test]
        public void DeveConseguirConsultarPorCpf()
        {
            dynamic obj = new ExpandoObject();
            obj.Cpf = "01356980813";
            var requisicao = new RequisicaoModel()
            {
                Id = Guid.NewGuid().ToString(),
                DataCadastro = DateTime.Now,
                ConteudoEntrada = new RequisicaoDataModel()
                {
                    TipoConteudo = "application/json",
                    Conteudo = obj
                }
            };



            Plugin.Execute(requisicao);
        }


        [Test]
        public void DeveConseguirValidarCpfInvalido()
        {
            dynamic obj = new ExpandoObject();
            obj.Cpf = "txt";
            var requisicao = new RequisicaoModel()
            {
                Id = Guid.NewGuid().ToString(),
                DataCadastro = DateTime.Now,
                ConteudoEntrada = new RequisicaoDataModel()
                {
                    TipoConteudo = "application/json",
                    Conteudo = obj
                }
            };



            Plugin.Execute(requisicao);
        }



    }
}
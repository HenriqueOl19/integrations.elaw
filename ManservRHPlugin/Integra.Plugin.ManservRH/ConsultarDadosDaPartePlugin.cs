﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Xml;

namespace Integra.Plugin.ManservRH
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("DisplayName", "ConsultarDadosDaParteManservPlugin")]
    [ExportMetadata("Description", "Consulta os dados do sistema do RH no sistema do cliente")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "Manserv_Consulta_Dados_RH")]
    [ExportMetadata("Status", PluginStatus.Ativo)]


    public class ConsultarDadosDaPartePlugin : IPlugin
    {
        public void Execute(RequisicaoModel requisicao)
        {
            Console.WriteLine($"Executando plugin ConsultarDadosDaParteManservPlugin, req id: {requisicao.Id.ToString()}");

            var conteudoEntrada = requisicao.ConteudoEntrada.Conteudo;

            var steps = StepsConfig();

            HttpHelper http = new HttpHelper(new WebClientBase());

            var respostaConsultaCpf = http.Post(steps.SingleOrDefault(x => x.Name == "ConsultarPorCpf"), new List<ExternalValues>()
            {
                new ExternalValues("{cpf}", conteudoEntrada.Cpf?.ToString())
            });

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(respostaConsultaCpf.Body);
            var xmlRetorno = doc.ChildNodes[1].ChildNodes[1].ChildNodes[0].InnerText;
            XmlDocument docInterno = new XmlDocument();
            docInterno.LoadXml(xmlRetorno);
            var campos = docInterno.ChildNodes[0].ChildNodes[0];
            if (campos == null)
            {
                throw new Common.Exceptions.BusinessException("Cpf inválido");
            }
            var codEmpresa = campos["CODEMPRESA"].InnerText;
            var codCusto = campos["CODCCUSTO"].InnerText;
            var nomeCusto = campos["NOMECCUSTO"].InnerText;
            var diretoria = campos["DIRETORIA"].InnerText;
            var nomeSituacao = campos["NOMESITUACAO"].InnerText;
            var tempoPermanencia = campos["TEMPOPERMANENCIA"].InnerText;
            var funcao = campos["FUNCAO"].InnerText;
            var re = campos["RE"].InnerText;
            var salario = campos["SALARIO"].InnerText;

            var conteudoRetorno = new
            {
                codEmpresa,
                codCusto,
                nomeCusto,
                diretoria,
                nomeSituacao,
                tempoPermanencia,
                funcao,
                re,
                salario
            };

            //insere o conteudo de retorno
            requisicao.ConteudoRetorno = new RequisicaoDataModel()
            {
                TipoConteudo = "application/json",
                Conteudo = conteudoRetorno
            };


        }

        private IList<HttpStep> StepsConfig()
        {
            IList<HttpStep> steps = new List<HttpStep>();

            steps.Add(new HttpStep("ConsultarPorCpf", "http://fluigh.manserv.com.br:82/TOTVSBusinessConnect/wsConsultaSQL.asmx", "POST", "UTF-8", HttpResultType.Xml)
            {
                ContentType = "application/soap+xml",
                Headers = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("SOAPAction","http://www.totvs.com.br/br/RealizarConsultaSQLAuth")
                },
                Body = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("", @"<?xml version='1.0' encoding='utf-8'?>
<soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'>
  <soap12:Body>
    <RealizarConsultaSQLAuth xmlns='http://www.totvs.com.br/br/'>
      <codSentenca>FLUIG_P_207</codSentenca>
      <codColigada>0</codColigada>
      <codAplicacao>p</codAplicacao>
      <Usuario>fluig</Usuario>
      <Senha>fluig</Senha>
      <parameters>CPF={cpf}</parameters>
    </RealizarConsultaSQLAuth>
  </soap12:Body>
</soap12:Envelope>")
                }
            }); ;
            return steps;
        }


    }
}

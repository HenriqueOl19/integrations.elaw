﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Xml;

namespace Integra.Plugin.CeASap
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("DisplayName", "EnviarPagamentoParaContabilizacaoCeASapPlugin")]
    [ExportMetadata("Description", "Envio de pagamento para o serviço de contabilização do cliente")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "CeA_SAP")]
    [ExportMetadata("Status", PluginStatus.Ativo)]
    public class EnviarPagamentoParaContabilizacao : IPlugin
    {
        public void Execute(RequisicaoModel requisicao)
        {
            Console.WriteLine($"Executando plugin EnviarPagamentoParaContabilizacaoCeASapPlugin, req id: {requisicao.Id.ToString()}");

            var conteudoEntrada = requisicao.ConteudoEntrada.Conteudo;

            var steps = StepsConfig();

            HttpHelper http = new HttpHelper(new WebClientBase());

            var respostaEnviaPagamento = http.Post(steps.SingleOrDefault(x => x.Name == "EnviarPagamento"), new List<ExternalValues>()
            {
                new ExternalValues("{cpf}", conteudoEntrada.Cpf?.ToString())
            });


            XmlDocument doc = new XmlDocument();
        }

        private IList<HttpStep> StepsConfig()
        {
            IList<HttpStep> steps = new List<HttpStep>();

            steps.Add(new HttpStep("EnviarPagamento", "http://10.19.209.61:6010/IntegracaoElaw/proxy/LancamentoContabilJuridicoPSv1", "POST", "UTF-8", HttpResultType.Xml)
            {
                ContentType = "application/soap+xml",
                Body = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("", @"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:con='http://cea.com.br/elaw/Contabilizar'>
   <Header/>
   <Body>
      <mtp_Elaw_Soap_Req>
         <ENTRADA>
            <IDPROC>{IDProc}</IDPROC>            
            <DATADOC>{DataDoc}</DATADOC>            
            <DATAVENC>{DataVenc}</DATAVENC>            
            <DOCREF>{DoCref}</DOCREF>            
            <EMPRESA>{Empresa}</EMPRESA>            
            <DIVISAO1>{Divisao1}</DIVISAO1>            
            <TXTITEM1>{TxtItem1}</TXTITEM1>            
            <FORNEC>{Fornec}</FORNEC>            
            <VALPAGTO>{ValPagto}</VALPAGTO>            
            <TXTCABEC>{TxtCabec}</TXTCABEC>            
            <SAPUSER>{Sapuser}</SAPUSER>            
            <CODBANCO>{CodBanco}</CODBANCO>            
            <AGENCIA>{Agendia}</AGENCIA>            
            <NUMCONTA>{NumConta}</NUMCONTA>            
            <OPERACAO>{Operacao}</OPERACAO>            
            <OPERACAO_DIG>{Operacao_Dig}</OPERACAO_DIG>            
            <CPF_CNPJ>{Cpf_Cnpj}</CPF_CNPJ>            
            <FAVORECIDO>{Favorecido}</FAVORECIDO>            
            <CENTROCUSTO>{CentroCusto}</CENTROCUSTO>            
            <DIVISAO2>{Divisao2}</DIVISAO2>            
            <TXTITEM2>{TxtItem2}</TXTITEM2>            
            <DESPESA>{Despesa}</DESPESA>            
            <TIPOPROC>{TipoProc}</TIPOPROC>            
            <OPERCONTAB>{OperContab}</OPERCONTAB>            
            <EVENTCONTAB>{EventContab}</EVENTCONTAB>            
            <DOC_SAP>{DocSap}</DOC_SAP>            
            <ANO>{Ano}</ANO>
		</ENTRADA>         
      </mtp_Elaw_Soap_Req>
   </Body>
</Envelope>")
                }
            });

            return steps;
        }
    }
}

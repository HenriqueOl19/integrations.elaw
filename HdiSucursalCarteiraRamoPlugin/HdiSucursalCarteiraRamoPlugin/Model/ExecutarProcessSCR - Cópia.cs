﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Xml;

namespace Integra.Plugin.HdiScrPlugin.model
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("Display", "ExecutarProcessSCR")]
    [ExportMetadata("Description", "HDI - Realiza o cadastro de Sucursal, Carteira e Ramo no Elaw")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "Hdi_Scr_Plugin")]
    [ExportMetadata("Status", PluginStatus.Ativo)]

    public class ExecutarProcessSCR : IPlugin
    {
        private readonly String _key = "AIzaSyBjMjZd81whWBYL1GNo1oL_fckSyJUD4q4";
        private readonly String _xApplicationId = "eLaw";
        private readonly String _xUserId = "elawbatch";
        private readonly String _xCompanyId = "01";
        private readonly String _clientId = "api-ins-elaw";
        private readonly String _client_secret = "16904e385204f226a13ca3bdea70829e1018bb8bd8073525e309134f20856443";
        private readonly String _grant_type = "client_credentials";


        public void Execute(RequisicaoModel requisicao)
        {
            try
            {

                Console.WriteLine($"Executando plugin HdiScrPlugin, req id: {requisicao.Id}");
                var conteudoEntrada = requisicao.ConteudoEntrada.Conteudo;
                HttpHelper http = new HttpHelper(new WebClientBase());



                //Para Testes 

                CompanyConversionsModel HDI = new CompanyConversionsModel(1, 142, "HDI", true, "01");
                CompanyConversionsModel Global = new CompanyConversionsModel(3, 122, "Global", true, "03");
                CompanyConversionsModel Santander = new CompanyConversionsModel(4, 101, "Santander", true, "04");
                List<CompanyConversionsModel> ListCompanyTeste = new List<CompanyConversionsModel> { HDI, Global, Santander };



                //EXECUTANDO INTEGRAÇÃO


                var client = new RestClient("https://openapi-int.hdi.com.br/corporate/business/v1/organizationBranches?companyId=03&api_key=AIzaSyBjMjZd81whWBYL1GNo1oL_fckSyJUD4q4");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("X-Application-Id", "eLaw");
                request.AddHeader("X-User-Id", "elawbatch");
                request.AddHeader("X-Company-Id", "01");
                request.AddHeader("Authorization", "Bearer eyJraWQiOiJiZDc2MzhjYzg4OTU2ZmNjZGFhMGRlYWQ2NDk5M2E1NjQ0ZTcwMTVkIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJoZGktY29ycG9yYXRlLWF1dGhAaW5zLWNyb3NzLXNlY3VyaXR5LWF1dGgtaW50Zy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6ImFwaS5oZGkuY29tLmJyIiwicm9sZXMiOlsiUm9sZTEiLCJSb2xlMiIsIlJvbGVOIl0sImlzcyI6ImhkaS1jb3Jwb3JhdGUtYXV0aEBpbnMtY3Jvc3Mtc2VjdXJpdHktYXV0aC1pbnRnLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiZXhwIjoxNTk2MTMwMzU0LCJpYXQiOjE1OTYxMjY3NTQsInVzZXIiOiJhcGktaW5zLWVsYXciLCJ3ZWJDbGllbnQiOiJhcGkuaGRpLmNvbS5iciJ9.S4g1_ux2sTTt4AxpMO_VXRuDiFCWxahCPyFKCh5As4In0bSd_JYNi5KrwD4tz7dldALppAWkoheTdtfPDIX0YLk3bTq0Bg3dlBs_qBfryS6Lbt9qflszo3TRX30RBSelVIUiL2ZlJ69ZYzPwBmnZ2Vosaz_DT9WmpRvhgoEguqCDLdm2NkWm1mzDx4Zu1Ae3NkuKZ7_jbthFM_60uMyD0rLiOH_IfYAkCUxoUal6hhlgnojmgbDF8Uq2ZQlY1zJvNEMk3AoJFtns-NBFx3wBsyXZG2-z083INOlpzGgtG8tXRWR8o9VNkXES1UkEc_ogKE-PFbLd5Gcpr54XmRMNtA");
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);

                //Chamando servico PostCode

                //Chamando Servico PostToken

                List<ResponseSucursal> listSucursal = new List<ResponseSucursal>();
                List<ResponseCarteira> listCarteira = new List<ResponseCarteira>();
                List<ResponseRamo> listRamo = new List<ResponseRamo>();


                var resultadoPostCodeJson = ChamarServicoCode(http);
                var resultadoPostTokenJson = ChamarServicoToken(http, resultadoPostCodeJson);
                listSucursal.AddRange(ChamarServicoSucursal2(http, ListCompanyTeste[1], resultadoPostTokenJson));
                listSucursal.AddRange(ChamarServicoSucursal2(http, ListCompanyTeste[2], resultadoPostTokenJson));
                listSucursal.AddRange(ChamarServicoSucursal(http, ListCompanyTeste[2], resultadoPostTokenJson));
                listCarteira.AddRange(ChamarServicoCarteira(http, ListCompanyTeste[0], resultadoPostTokenJson));


                ResponseSCRModel Response = new ResponseSCRModel(200, listSucursal, listCarteira, listRamo);


                requisicao.ConteudoRetorno.Conteudo = Response;


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Execute(new RequisicaoModel());
            }
        }


        public String ChamarServicoCode(HttpHelper http)
        {
            var jsonCode = "{\u0022clientId\u0022: \u0022" + _clientId + "\u0022, \u0022clientSecret\u0022: \u0022" + _client_secret + "\u0022, \u0022grantType\u0022: \u0022" + _grant_type + "\u0022 }";
            var resultadoPostCode = (HttpResultJson)http.Post(StepsConfig().FirstOrDefault(x => x.Name == "PostCode"), new List<ExternalValues>()
                    {
                        new ExternalValues ("{Key}", _key),
                        new ExternalValues ("{jsonCode}",jsonCode)
                    });


            //Serializa JSON CODE
            //JObject Resposta = JObject.Parse(resultadoPostCode.Body);
            //ResponseToken RespostaPostCode = JsonConvert.DeserializeObject<ResponseToken>(resultadoPostCode.Body);
            //var resultadoPostCodeJson = RespostaPostCode.code;
            return resultadoPostCode.Body;

        }

        public String ChamarServicoToken(HttpHelper http, string resultadoPostCodeJson)
        {
            var resultadoPostToken = (HttpResultJson)http.Post(StepsConfig().FirstOrDefault(x => x.Name == "PostToken"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Key}", _key),
                    new ExternalValues("{code}",resultadoPostCodeJson)
                });

            var codeServiçoToken = ((Newtonsoft.Json.Linq.JValue)((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)resultadoPostToken.Json).First).Value).Value;
            if (codeServiçoToken.ToString().Contains("400"))
            {
                throw new Exception(((Newtonsoft.Json.Linq.JValue)((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)resultadoPostToken.Json).First.Next).Value).Value.ToString());
            }

            return ((Newtonsoft.Json.Linq.JValue)((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)resultadoPostToken.Json).First).Value).Value.ToString();
        }

        public IList<ResponseSucursal> ChamarServicoSucursal(HttpHelper http, CompanyConversionsModel item, String token)
        {
            var resultadoGetSucursal = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "GetSucursal"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Api_key}", _key),
                    new ExternalValues ("{CompanyId}", item.companyFromClient),
                    new ExternalValues ("{Token}", "Bearer " +token)
                });

            JArray sucursais = JArray.Parse(resultadoGetSucursal.Body);
            IList<ResponseSucursal> listaSucursal = new List<ResponseSucursal>();

            for (int i = 0; i < sucursais.Count; i++)
            {
                JObject respostaSucursal = JObject.Parse(sucursais[i].ToString());
                ResponseSucursal responseSucursal = JsonConvert.DeserializeObject<ResponseSucursal>(respostaSucursal.ToString());
                //responseSRC.
                listaSucursal.Add(responseSucursal);
            }

            return listaSucursal;
        }


        public IList<ResponseSucursal> ChamarServicoSucursal2(HttpHelper http, CompanyConversionsModel item, String token)
        {
            var resultadoGetSucursal = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "GetSucursal"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Api_key}", _key),
                    new ExternalValues ("{CompanyId}", "01"),
                    new ExternalValues ("{Token}", "Bearer " +token)
                });

            var resultadoGetSucursal2 = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "GetSucursal"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Api_key}", _key),
                    new ExternalValues ("{CompanyId}", "01"),
                    new ExternalValues ("{Token}", "Bearer " +token)
                });


            JArray sucursais = JArray.Parse(resultadoGetSucursal.Body);
            IList<ResponseSucursal> listaSucursal = new List<ResponseSucursal>();

            for (int i = 0; i < sucursais.Count; i++)
            {
                JObject respostaSucursal = JObject.Parse(sucursais[i].ToString());
                ResponseSucursal responseSucursal = JsonConvert.DeserializeObject<ResponseSucursal>(respostaSucursal.ToString());
                //responseSRC.
                listaSucursal.Add(responseSucursal);
            }

            return listaSucursal;
        }


        private IList<ResponseCarteira> ChamarServicoCarteira(HttpHelper http, CompanyConversionsModel item, string token)
        {
            var resultadoGetCarteira = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "GetCarteira"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Api_key}", _key),
                    new ExternalValues ("{CompanyId}", item.companyFromClient),
                    new ExternalValues ("{Token}", "Bearer " +token)
                });
            JArray carteira = JArray.Parse(resultadoGetCarteira.Body);
            IList<ResponseCarteira> listCarteira = new List<ResponseCarteira>();

            for (int i = 0; i < carteira.Count; i++)
            {
                JObject respostaCarteira = JObject.Parse(carteira[i].ToString());
                ResponseCarteira responseCarteira = JsonConvert.DeserializeObject<ResponseCarteira>(respostaCarteira.ToString());
                listCarteira.Add(responseCarteira);
            }
            return listCarteira;

        }

        private IList<ResponseRamo> ChamarServicoRamo(HttpHelper http, CompanyConversionsModel item, string token)
        {
            var resultadoGetRamo = (HttpResultJson)http.Get(StepsConfig().FirstOrDefault(x => x.Name == "GetRamo"), new List<ExternalValues>()
                {
                    new ExternalValues ("{Api_key}", _key),
                    new ExternalValues ("{CompanyId}", item.companyFromClient),
                    new ExternalValues ("{Token}", "Bearer " +token)
                });
            JArray ramo = JArray.Parse(resultadoGetRamo.Body);
            IList<ResponseRamo> listRamo = new List<ResponseRamo>();

            for (int i = 0; i < ramo.Count; i++)
            {
                JObject respostaRamo = JObject.Parse(ramo[i].ToString());
                ResponseRamo responseRamo = JsonConvert.DeserializeObject<ResponseRamo>(respostaRamo.ToString());
                listRamo.Add(responseRamo);
            }
            return listRamo;


        }

        private IList<HttpStep> StepsConfig()
        {


            IList<HttpStep> steps = new List<HttpStep>();

            steps.Add(new HttpStep("PostCode", "https://openapi-int.hdi.com.br/corporate/security/v1/authorize?key=AIzaSyBjMjZd81whWBYL1GNo1oL_fckSyJUD4q4", "POST", "UTF-8", HttpResultType.Json)
            {

                ContentType = "application/json",
                Headers = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("X-Application-Id",_xApplicationId),
                    new HttpKeyValue("X-User-Id",_xUserId),
                    new HttpKeyValue("X-Company-Id",_xCompanyId),
                },
                Body = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("","{jsonCode}")
                }
            });

            steps.Add(new HttpStep("PostToken", "https://openapi-int.hdi.com.br/corporate/security/v1/token?key={Key}", "POST", "UTF-8", HttpResultType.Json)
            {

                ContentType = "application/json",
                Headers = new List<HttpKeyValue>()

                {
                    new HttpKeyValue("X-Application-Id",_xApplicationId),
                    new HttpKeyValue("X-User-Id",_xUserId),
                    new HttpKeyValue("X-Company-Id",_xCompanyId),
                    //new HttpKeyValue("Content-Type",ContentType)
                },
                Body = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("code","{code}")
                }
            });

            steps.Add(new HttpStep("GetSucursal", "https://openapi-int.hdi.com.br/corporate/business/v1/organizationBranches?companyId={CompanyId}&api_key={Api_key}", "GET", "UTF-8", HttpResultType.Json)
            {
                ContentType = "application/json",
                Headers = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("X-Application-Id",_xApplicationId),
                    new HttpKeyValue("X-User-Id",_xUserId),
                    new HttpKeyValue("X-Company-Id",_xCompanyId),
                    new HttpKeyValue("Authorization","{Token}")
                }
            });

            steps.Add(new HttpStep("GetCarteira", "https://openapi-int.hdi.com.br/corporate/product/v1/products?companyId={CompanyId}&api_key={Api_key}", "GET", "UTF-8", HttpResultType.Json)
            {
                ContentType = "application/json",
                Headers = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("X-Application-Id",_xApplicationId),
                    new HttpKeyValue("X-User-Id",_xUserId),
                    new HttpKeyValue("X-Company-Id",_xCompanyId),
                    new HttpKeyValue("Authorization","{Token}")
                }
            });

            steps.Add(new HttpStep("GetRamo", "https://openapi-int.hdi.com.br/corporate/product/v1/lines?companyId={CompanyId}&api_key={Api_key}", "GET", "UTF-8", HttpResultType.Json)
            {

                ContentType = "application/json",
                Headers = new List<HttpKeyValue>()
                {
                   new HttpKeyValue("X-Application-Id",_xApplicationId),
                    new HttpKeyValue("X-User-Id",_xUserId),
                    new HttpKeyValue("X-Company-Id",_xCompanyId),
                    new HttpKeyValue("Authorization","{Token}")
                }
            });


            return steps;
        }

    }


}


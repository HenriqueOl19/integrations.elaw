﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integra.Plugin.HdiScrPlugin
{
    public class ResponseSCRModel
    {

        public int code { get; set; }
        public List<ResponseSucursal> sucursal { get; set; }
        public List<ResponseCarteira> carteira { get; set; }
        public List<ResponseRamo> ramo { get; set; }
        public ResponseSCRModel(int code, List<ResponseSucursal> sucursal, List<ResponseCarteira> carteira, List<ResponseRamo> ramo)
        {
            this.code = code;
            this.sucursal = sucursal;
            this.carteira = carteira;
            this.ramo = ramo;
        }
    }

    


    public class ResponseSucursal
    {
        public int companyId { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public String companyIdFromClient { get; set; }
    }

    public class ResponseCarteira
    {
        public int companyId { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public string companyIdFromClient { get; set; }
    }

    public class ResponseRamo
    {
        public int companyId { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public string companyIdFromClient { get; set; }
    }

    public class ResponseToken
    {
        public string token { get; set; }
        public string tokenType { get; set; }
        public string expiresIn { get; set; }
        public string refreshToken { get; set; }
        public string scope { get; set; }
    }


    
}

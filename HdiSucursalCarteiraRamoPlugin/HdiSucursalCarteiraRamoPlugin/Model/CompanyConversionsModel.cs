﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integra.Plugin.HdiScrPlugin
{
    public class CompanyConversionsModel
    {
        public int companyFrom { get; set;}
        public int companyTo { get; set; }
        public String label { get; set; }
        public bool isActive { get; set; }
        public string companyFromClient { get; set; }


        public CompanyConversionsModel(int _companyFrom, int _companyTo, string _label, bool _isActive, string _companyFromClient) 
        {
            companyFrom = _companyFrom;
            companyTo = _companyTo;
            label = _label;
            isActive = _isActive;
            companyFromClient = _companyFromClient;

        }
    }
}

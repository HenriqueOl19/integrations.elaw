﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Xml;

namespace Integra.Plugin.HdiScrPlugin
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("Display", "ExecutarProcessSCR")]
    [ExportMetadata("Description", "HDI - Realiza o cadastro de Sucursal, Carteira e Ramo no Elaw")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "Hdi_Scr_Plugin")]
    [ExportMetadata("Status", PluginStatus.Ativo)]

    public class ExecutarProcessSCR : IPlugin
    {
        private readonly String _key = "AIzaSyBjMjZd81whWBYL1GNo1oL_fckSyJUD4q4";
        private readonly String _xApplicationId = "eLaw";
        private readonly String _xUserId = "elawbatch";
        private readonly String _xCompanyId = "01";
        private readonly String _clientId = "api-ins-elaw";
        private readonly String _client_secret = "16904e385204f226a13ca3bdea70829e1018bb8bd8073525e309134f20856443";
        private readonly String _grant_type = "client_credentials";
        private readonly String _contentType = "application/json";


        public void Execute(RequisicaoModel requisicao)
        {
            try
            {
                Console.WriteLine($"Executando plugin HdiScrPlugin, req id: {requisicao.Id}");
                var conteudoEntrada = requisicao.ConteudoEntrada.Conteudo;
                HttpHelper http = new HttpHelper(new WebClientBase());

                //Para Testes 
                CompanyConversionsModel HDI = new CompanyConversionsModel(1, 142, "HDI", true, "01");
                CompanyConversionsModel Global = new CompanyConversionsModel(3, 122, "Global", true, "03");
                CompanyConversionsModel Santander = new CompanyConversionsModel(4, 101, "Santander", true, "04");
                List<CompanyConversionsModel> ListCompany = new List<CompanyConversionsModel> { HDI, Global, Santander };

                //EXECUTANDO INTEGRAÇÃO
                List<ResponseSucursal> listSucursal = new List<ResponseSucursal>();
                List<ResponseCarteira> listCarteira = new List<ResponseCarteira>();
                List<ResponseRamo> listRamo = new List<ResponseRamo>();

                //Chamando servico PostCode
                var resultadoPostCodeJson = ChamarServicoCode(http);

                //Chamando Servico PostToken
                var resultadoPostTokenJson = ChamarServicoToken(http, resultadoPostCodeJson);

                //Chamando os serviços de Sucursal, Carteira e Ramo
                foreach (CompanyConversionsModel item in ListCompany)
                {
                    listSucursal.AddRange(ChamarServicoSucursal(http, item, resultadoPostTokenJson));
                    listCarteira.AddRange(ChamarServicoCarteira(http, item, resultadoPostTokenJson));
                    listRamo.AddRange(ChamarServicoRamo(http, item, resultadoPostTokenJson)); // Serviço fora do ar 30.07
                }


                //retorna no json Requisicao
                ResponseSCRModel Response = new ResponseSCRModel(200, listSucursal, listCarteira, listRamo);
                requisicao.ConteudoRetorno.Conteudo = Response;


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                dynamic retorno = "{\"code\" : 500 , \n\"Message\" : \"" + e.Message.Replace("\"", "'").Replace("{", "").Replace("}", "") + "\"\n}";
                requisicao.ConteudoRetorno.Conteudo = retorno;
            }
        }


        public String ChamarServicoCode(HttpHelper http)
        {
            var chamadaServicoCode = new RestClient("https://openapi-int.hdi.com.br/corporate/security/v1/authorize?key=" + _key);
            chamadaServicoCode.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("X-Application-Id", _xApplicationId);
            request.AddHeader("X-User-Id", _xUserId);
            request.AddHeader("X-Company-Id", _xCompanyId);
            request.AddHeader("Content-Type", _contentType);
            request.AddParameter("application/json", "{\r\n    \"clientId\": \"" + _clientId + "\",\r\n    \"clientSecret\": \"" + _client_secret + "\",\r\n    \"grantType\": \"" + _grant_type + "\"\r\n}", ParameterType.RequestBody);
            IRestResponse responseServicoCode = chamadaServicoCode.Execute(request);

            ValidarChamadaServiço(responseServicoCode.StatusCode.ToString(), responseServicoCode.StatusDescription);
            return responseServicoCode.Content;

        }

        public String ChamarServicoToken(HttpHelper http, string resultadoPostCodeJson)
        {
            var chamadaServicoToken = new RestClient("https://openapi-int.hdi.com.br/corporate/security/v1/token?key=" + _key);
            chamadaServicoToken.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("X-Application-Id", _xApplicationId);
            request.AddHeader("X-User-Id", _xUserId);
            request.AddHeader("X-Company-Id", _xCompanyId);
            request.AddHeader("Content-Type", _contentType);
            request.AddParameter("application/json", resultadoPostCodeJson, ParameterType.RequestBody);
            IRestResponse responseServicoToken = chamadaServicoToken.Execute(request);

            //transforma o JSON num objeto
            JObject respostaServicoToken = JObject.Parse(responseServicoToken.Content);
            ResponseToken responseToken = JsonConvert.DeserializeObject<ResponseToken>(respostaServicoToken.ToString());

            ValidarChamadaServiço(responseServicoToken.StatusCode.ToString(), responseServicoToken.StatusDescription);
            return responseToken.token;

        }

        public IList<ResponseSucursal> ChamarServicoSucursal(HttpHelper http, CompanyConversionsModel item, String token)
        {
            var chamadaServicoSucursal = new RestClient("https://openapi-int.hdi.com.br/corporate/business/v1/organizationBranches?companyId=" + item.companyFromClient + "&api_key=" + _key);
            chamadaServicoSucursal.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-Application-Id", _xApplicationId);
            request.AddHeader("X-User-Id", _xUserId);
            request.AddHeader("X-Company-Id", _xCompanyId);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddParameter("text/plain", "", ParameterType.RequestBody);
            IRestResponse responseServicoSucursal = chamadaServicoSucursal.Execute(request);

            ValidarChamadaServiço(responseServicoSucursal.StatusCode.ToString(), responseServicoSucursal.StatusDescription + " - " + responseServicoSucursal.Content);

            JArray sucursais = JArray.Parse(responseServicoSucursal.Content);
            IList<ResponseSucursal> listaSucursal = new List<ResponseSucursal>();

            for (int i = 0; i < sucursais.Count; i++)
            {
                JObject respostaSucursal = JObject.Parse(sucursais[i].ToString());
                ResponseSucursal responseSucursal = JsonConvert.DeserializeObject<ResponseSucursal>(respostaSucursal.ToString());
                responseSucursal.companyIdFromClient = item.companyFromClient;
                listaSucursal.Add(responseSucursal);
            }

            return listaSucursal;
        }

        private IList<ResponseCarteira> ChamarServicoCarteira(HttpHelper http, CompanyConversionsModel item, string token)
        {
            var chamadaServicoCarteira = new RestClient("https://openapi-int.hdi.com.br/corporate/product/v1/products?companyId=" + item.companyFromClient + "&api_key=" + _key);
            chamadaServicoCarteira.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-Application-Id", _xApplicationId);
            request.AddHeader("X-User-Id", _xUserId);
            request.AddHeader("X-Company-Id", _xCompanyId);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddParameter("text/plain", "", ParameterType.RequestBody);
            IRestResponse responseServicoCarteira = chamadaServicoCarteira.Execute(request);

            ValidarChamadaServiço(responseServicoCarteira.StatusCode.ToString(), responseServicoCarteira.StatusDescription + " - " + responseServicoCarteira.Content);


            JArray carteira = JArray.Parse(responseServicoCarteira.Content);
            IList<ResponseCarteira> listCarteira = new List<ResponseCarteira>();

            for (int i = 0; i < carteira.Count; i++)
            {
                JObject respostaCarteira = JObject.Parse(carteira[i].ToString());
                ResponseCarteira responseCarteira = JsonConvert.DeserializeObject<ResponseCarteira>(respostaCarteira.ToString());
                responseCarteira.companyIdFromClient = item.companyFromClient;
                listCarteira.Add(responseCarteira);
            }
            return listCarteira;

        }

        private IList<ResponseRamo> ChamarServicoRamo(HttpHelper http, CompanyConversionsModel item, string token)
        {
            var chamadaServicoRamo = new RestClient("https://openapi-int.hdi.com.br/corporate/product/v1/lines?companyId=" + item.companyFromClient + "&api_key=" + _key);
            chamadaServicoRamo.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-Application-Id", _xApplicationId);
            request.AddHeader("X-User-Id", _xUserId);
            request.AddHeader("X-Company-Id", _xCompanyId);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddParameter("text/plain", "", ParameterType.RequestBody);
            IRestResponse responseServicoRamo = chamadaServicoRamo.Execute(request);

            ValidarChamadaServiço(responseServicoRamo.StatusCode.ToString(), responseServicoRamo.StatusDescription + " - " + responseServicoRamo.Content);

            JArray ramo = JArray.Parse(responseServicoRamo.Content);
            IList<ResponseRamo> listRamo = new List<ResponseRamo>();

            for (int i = 0; i < ramo.Count; i++)
            {
                JObject respostaRamo = JObject.Parse(ramo[i].ToString());
                ResponseRamo responseRamo = JsonConvert.DeserializeObject<ResponseRamo>(respostaRamo.ToString());
                responseRamo.companyIdFromClient = item.companyFromClient;
                listRamo.Add(responseRamo);
            }
            return listRamo;
        }


        /// <summary>
        /// Função para validar o sucesso do serviço
        /// </summary>
        /// <param name="content"></param>
        /// <param name="statusDescription"></param>
        public void ValidarChamadaServiço(string content, string statusDescription)
        {
            if (content != "200" && content != "OK")
            {
                throw new Exception(statusDescription);
            }

        }
       
    }


}


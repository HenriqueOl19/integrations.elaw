﻿using Integra.Common.Model;
using Integra.Common.Plugin;
using System;
using System.ComponentModel.Composition;

namespace Integra.Plugin.CarrefourDocusign
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("DisplayName", "EnviarArquivoDocusign")]
    [ExportMetadata("Description", "Montar e enviar contratos para docusign")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "Carrefour_DocuSign_EnviarDocumentos")]
    [ExportMetadata("Status", PluginStatus.Ativo)]
    public class EnviarDocumento : IPlugin
    {
        public void Execute(RequisicaoModel requisicao)
        {
            throw new NotImplementedException();
        }
    }
}

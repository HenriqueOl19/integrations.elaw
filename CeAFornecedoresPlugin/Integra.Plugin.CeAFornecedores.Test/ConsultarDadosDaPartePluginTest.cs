using Integra.Common.Model;
using NUnit.Framework;
using System;
using System.Dynamic;

namespace Integra.Plugin.CeAFornecedores.Test
{
    public class Tests
    {
        private ConsultarDadosDaPartePlugin Plugin;

        [SetUp]
        public void Setup()
        {
            Plugin = new ConsultarDadosDaPartePlugin();
        }

        [Test]
        public void DeveConseguirConsultarPorCpf()
        {
            dynamic obj = new ExpandoObject();
            obj.Cpf_Cnpj = "01356980813123";
            var requisicao = new RequisicaoModel()
            {
                Id = Guid.NewGuid().ToString(),
                DataCadastro = DateTime.Now,
                ConteudoEntrada = new RequisicaoDataModel()
                {
                    TipoConteudo = "application/json",
                    Conteudo = obj
                }
            };



            Plugin.Execute(requisicao);
        }

    }
}
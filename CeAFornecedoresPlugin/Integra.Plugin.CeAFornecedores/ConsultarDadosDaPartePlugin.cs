﻿using Integra.Common.Helpers.Http;
using Integra.Common.Model;
using Integra.Common.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace Integra.Plugin.CeAFornecedores
{
    [Export(typeof(IPlugin))]
    [ExportMetadata("DisplayName", "ConsultarDadosDaParteCeAFornecedoresPlugin")]
    [ExportMetadata("Description", "Consulta os dados do sistema de fornecedores no sistema do cliente")]
    [ExportMetadata("Version", "1.1")]
    [ExportMetadata("Queue", "CeA_Consulta_Fornecedores")]
    [ExportMetadata("Status", PluginStatus.Ativo)]
    public class ConsultarDadosDaPartePlugin : IPlugin
    {
        public void Execute(RequisicaoModel requisicao)
        {
            Console.WriteLine($"Executando plugin ConsultarDadosDaParteCeAFornecedoresPlugin, req id: {requisicao.Id.ToString()}");

            var conteudoEntrada = requisicao.ConteudoEntrada.Conteudo;

            var steps = StepsConfig();

            HttpHelper http = new HttpHelper(new WebClientBase());



            if (conteudoEntrada.Cpf_Cnpj?.ToString().Length < 14)
            {
                var respostaConsultaCpf = http.Post(steps.SingleOrDefault(x => x.Name == "ConsultarFornecedoresPorCpf"), new List<ExternalValues>()
                {
                    new ExternalValues("{cpf}", conteudoEntrada.Cpf_Cnpj?.ToString()),
                });
            }
            else
            {
                var respostaConsultaCpf = http.Post(steps.SingleOrDefault(x => x.Name == "ConsultarFornecedoresPorCpf"), new List<ExternalValues>()
                {
                    new ExternalValues("{cnpj}", conteudoEntrada.Cpf_Cnpj?.ToString()),
                });
            }


        }

        private IList<HttpStep> StepsConfig()
        {
            IList<HttpStep> steps = new List<HttpStep>();

            steps.Add(new HttpStep("ConsultarFornecedoresPorCpf", "", "POST", "UTF-8", HttpResultType.Xml)
            {

                ContentType = "application/soap+xml",
                Body = new List<HttpKeyValue>()
                {
                    new HttpKeyValue("",@"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:con='http://www.cea.com.br/ConsultarFornecedor_v1/'>
   <soapenv:Header/>
   <soapenv:Body>
      <con:ConsultarFornecedorRequest>
         <nomeFornecedor></nomeFornecedor>
         <cnpj>{cnpj}</cnpj>
         <cpf>{cpf}</cpf>
         <numeroConta></numeroConta>
         <empresa></empresa>
         <organizacao></organizacao>
      </con:ConsultarFornecedorRequest>
   </soapenv:Body>
</soapenv:Envelope>")
                },

            });

            return steps;
        }
    
    
        
    
    
    
    
    }
}
